﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public enum TeleportEnum
    {
        GoTeleport,
        BackTeleport,
        GoTo4scene,
        GoTo2scene
    }


    [SerializeField]
    private PlayerController _playerController;
    [SerializeField]
    private GameObject[] points;
    [SerializeField]
    private GameObject activeScene;

    private int index = 0;

    public static GameController instance;

    void Awake()
    {
        instance = this;
        activeScene = Instantiate(points[index], transform.parent);

        var animator = activeScene.GetComponentInChildren<Animator>();
        Debug.Log(animator);
        //_playerControllerX.Init(animator); 
    }

    public void Teleport(TeleportEnum teleportEnum)
    {
        switch (teleportEnum)
        {
            case TeleportEnum.GoTeleport:
                GoTeleport();
                break;
            case TeleportEnum.BackTeleport:
                BackTeleport();
                break;
            case TeleportEnum.GoTo4scene:
                GoTo4scene();
                break;
            case TeleportEnum.GoTo2scene:
                GoTo2scene();
                break;
        }
    }

    public void GoTeleport()
    {
        index++;
        if (index >= points.Length)
        {
            index = 0;
        }
        Destroy(activeScene);
        activeScene = Instantiate(points[index], transform.parent);
    }

    public void BackTeleport()
    {
        index--;
        if (index >= points.Length)
        {
            index = 0;
        }
        if (index < 0)
        {
            index = 0;
        }
        Destroy(activeScene);
        activeScene = Instantiate(points[index], transform.parent);
    }
    public void GoTo4scene()
    {
        index = 3;
        if (index >= points.Length)
        {
            index = 0;
        }
        Destroy(activeScene);
        activeScene = Instantiate(points[index], transform.parent);
    }
    public void GoTo2scene()
    {
        index = 1;
        if (index >= points.Length)
        {
            index = 0;
        }
        Destroy(activeScene);
        activeScene = Instantiate(points[index], transform.parent);
    }
}
