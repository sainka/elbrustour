﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private Camera camera;
    private Animation anim;

    //public Animator animator; 
    [SerializeField]
    private AudioClip _click;

    GameController gameController;

    [SerializeField]
    private Material fadeMat;
    [SerializeField]
    private GameObject fadeObj;

    private float _speedFade = 20f;

    private bool key = false;
    private float time;

    //public void Init(Animator _animator) 
    //{ 
    // animator = _animator; 
    //} 

    void Start()
    {
        if (camera == null)
        {
            camera = Camera.main;
        }
    }

    void Update()
    {
        RaycastHit hit;
        Ray ray;
        ray = new Ray(camera.transform.position, camera.transform.forward);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.tag == "Sphere")
            {
                OVRInput.Update();
                if (Input.GetKeyDown(KeyCode.Space) || OVRInput.GetDown(OVRInput.Touch.PrimaryTouchpad) || (Input.GetMouseButtonDown(0)))
                {
                    StartCoroutine(FadeInOut(GameController.TeleportEnum.GoTeleport));
                    AudioSource.PlayClipAtPoint(_click, new Vector3(0, 0, 0));
                }
            }
            if (hit.collider.tag == "SphereBack")
            {
                if (Input.GetKeyDown(KeyCode.Space) || OVRInput.GetDown(OVRInput.Touch.PrimaryTouchpad) || (Input.GetMouseButtonDown(0)))
                {
                    StartCoroutine(FadeInOut(GameController.TeleportEnum.BackTeleport));
                    AudioSource.PlayClipAtPoint(_click, new Vector3(0, 0, 0));
                }
            }
            if (hit.collider.tag == "SphereTo4")
            {
                if (Input.GetKeyDown(KeyCode.Space) || OVRInput.GetDown(OVRInput.Touch.PrimaryTouchpad) || (Input.GetMouseButtonDown(0)))
                {
                    StartCoroutine(FadeInOut(GameController.TeleportEnum.GoTo4scene));
                    AudioSource.PlayClipAtPoint(_click, new Vector3(0, 0, 0));
                }
            }
            if (hit.collider.tag == "SphereTo2")
            {
                if (Input.GetKeyDown(KeyCode.Space) || OVRInput.GetDown(OVRInput.Touch.PrimaryTouchpad) || (Input.GetMouseButtonDown(0)))
                {
                    StartCoroutine(FadeInOut(GameController.TeleportEnum.GoTo2scene));
                    AudioSource.PlayClipAtPoint(_click, new Vector3(0, 0, 0));
                }
            }
        }
    }
    private IEnumerator FadeInOut(GameController.TeleportEnum teleportEnum)
    {
        fadeObj.SetActive(true);
        float i = 0;
        while (i < 1)
        {
            i += Time.deltaTime;
            fadeMat.color = new Color(0, 0, 0, i);
            yield return null;
        }
        GameController.instance.Teleport(teleportEnum);
        yield return new WaitForSeconds(0.1f);
        while (i > 0)
        {
            i -= Time.deltaTime;
            fadeMat.color = new Color(0, 0, 0, i);
            yield return null;
        }
        fadeObj.SetActive(false);
    }
}