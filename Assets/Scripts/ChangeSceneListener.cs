﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.SceneManagement;

//public class ChangeSceneListener : MonoBehaviour
//{
//    [SerializeField]
//    private List<string> sceneNames = new List<string> { "Everest", "Everest2", "Everest3", "Everest4", "Everest5" };
//    private Transform _camTransform;
//    [SerializeField]
//    private GameObject loadPrefub;

//    public static ChangeSceneListener Instance;
//    // Use this for initialization
//    private void Awake()
//    {
//        if (Instance != this)
//        {
//            if (Instance != null)
//            {
//                Destroy(Instance.gameObject);
//            }
//            Instance = this;
//        }
//        if (Instance == null)
//        {
//            Instance = this;
//        }
//        DontDestroyOnLoad(this.gameObject);
//    }
//    void OnEnable()
//    {
//        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
//        SceneManager.sceneLoaded += OnLevelFinishedLoading;
//    }

//    void OnDisable()
//    {
//        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
//        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
//    }
//    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
//    {
//        Debug.Log("Level Loaded - " + scene.name);
//        if (FindObjectOfType<OVRCameraRig>() != null)                    //загрузка сцен
//        {
//            _camTransform = FindObjectOfType<OVRCameraRig>().transform;
//            GameObject loadObject = Instantiate(loadPrefub);

//            loadPrefub.transform.SetParent(_camTransform);
//            loadPrefub.transform.localPosition = Vector3.zero;
//            loadObject.transform.localEulerAngles = Vector3.zero;
//        }
//    }
//}
