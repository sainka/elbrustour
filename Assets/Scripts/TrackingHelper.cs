﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingHelper : MonoBehaviour
{

#if UNITY_EDITOR
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftAlt))
        {
            transform.Rotate(Vector3.up * Input.GetAxis("Mouse X"), Space.World);
            transform.Rotate(Vector3.right * Input.GetAxis("Mouse Y") * -1);
        }
    }
#endif
}
