﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportObj : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    private void OnTriggerEnter(Collider other)
    {
        animator.SetTrigger("TeleportTrigger");
    }

    private void OnTriggerExit(Collider other)
    {
        animator.SetTrigger("ExitTeleportTrigger");
    }
}
